# Simple Url parser

A simple url parser.

## Usage
    $url = 'http://www.example.com';
    $parser = new UrlParser(new UrlValidator());
    $urlObject = $parser->parseUrl($url);
    $urlObject->getFullUrl(); // returns "http://www.example.com"