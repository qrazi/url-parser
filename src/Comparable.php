<?php

namespace Sivlingworkz;

/**
 * @link https://wiki.php.net/rfc/comparable
 *
 * Interface to make the comparison of objects more structured and allowing
 * a more specific comparison then "==" or "===" can do.
 */
interface Comparable
{
    /**
     * Should return -1 if $this is less then $other, 1 if $this is more then
     * $other, and 0 if $this is equal to $other.
     *
     * @param $other
     * @return boolean
     */
    public function compareTo($other);
}