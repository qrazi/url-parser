<?php

namespace Sivlingworkz\Validation;


interface Validator
{
    /**
     * @param $input
     * @throws ValidationException
     * @return boolean
     */
    public function validate($input);
}