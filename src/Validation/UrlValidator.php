<?php

namespace Sivlingworkz\Validation;


class UrlValidator implements Validator
{
    /**
     * @inheritDoc
     */
    public function validate($input)
    {
        // If first char is a forward slash, validate as relative url
        if (strpos($input, '/') === 0) {
            // Use standard filter_var to validate if proper url
            return (filter_var($input, FILTER_VALIDATE_URL,
                    FILTER_FLAG_PATH_REQUIRED) !== false);
        }

        // If string starts with http/https and has ://, assume absolute url
        if (preg_match('/^(?:http:\/\/|https:\/\/).+/i', $input)) {
            // Use standard filter_var to validate if proper url
            return (filter_var($input, FILTER_VALIDATE_URL,
                    FILTER_FLAG_SCHEME_REQUIRED | FILTER_FLAG_HOST_REQUIRED | FILTER_FLAG_PATH_REQUIRED) !== false);
        }

        // Not relative or absolute url
        throw new ValidationException('Could not validate "' . $input . '" as relative or absolute url');
    }

}