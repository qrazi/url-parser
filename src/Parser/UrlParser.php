<?php

namespace Sivlingworkz\Parser;

use Sivlingworkz\Validation\ValidationException;
use Sivlingworkz\Validation\Validator;
use Sivlingworkz\ValueObject\Url;

class UrlParser
{
    /**
     * @var Validator
     */
    private $urlValidator;

    /**
     * @param Validator $urlValidator
     */
    public function __construct(Validator $urlValidator){
        $this->urlValidator = $urlValidator;
    }

    /**
     * @param $urlString
     * @throws ParseException
     * @return Url
     */
    public function parseUrl($urlString)
    {
        try {
            // First validate the URL to see if supported by the parser
            $this->urlValidator->validate($urlString);
        } catch (ValidationException $e){
            throw new ParseException('Malformed url, could not parse', -1, $e);
        }

        // Rely on standard PHP functionality for initial parsing.
        $parts = parse_url($urlString);

        $host = $this->nullCoalesce($parts, 'host');

        if ($host) {
            // Dealing with a full URL
            return $this->createFullUrl($parts);
        }
        // Dealing with a relative URL
        return $this->createRelativeUrl($parts);
    }

    /**
     *
     * @param array $parts
     * @throws ParseException Scheme and host are required for a full URL
     * @return Url
     */
    private function createFullUrl(array $parts)
    {
        // Create an URL valueobject, which represents the full URL.
        $url = new Url(
            $this->requireValue($parts, 'scheme'),
            $this->requireValue($parts, 'host'),
            $this->nullCoalesce($parts, 'port'),
            $this->nullCoalesce($parts, 'user'),
            $this->nullCoalesce($parts, 'pass'),
            $this->normalizePath(
                $this->nullCoalesce($parts, 'path')
            ),
            $this->nullCoalesce($parts, 'query'),
            $this->nullCoalesce($parts, 'fragment')
        );

        return $url;
    }

    /**
     *
     * @param array $parts
     * @throws ParseException Path is required for a relative URL
     * @return Url
     */
    private function createRelativeUrl(array $parts)
    {
        // Create an URL valueobject, which represents the full URL.
        $url = new Url(
            null,
            null,
            null,
            null,
            null,
            $this->normalizePath(
                $this->requireValue($parts, 'path')
            ),
            $this->nullCoalesce($parts, 'query'),
            $this->nullCoalesce($parts, 'fragment')
        );

        return $url;
    }

    /**
     * Normalize the path of an url.
     *
     * @param $path Path-part of an URL
     * @return mixed
     */
    private function normalizePath($path)
    {
        if (!$path) {
            // $path is a falsy value, so nothing to normalize
            return $path;
        }

        // Remove single dot references, those just refer to current path.
        $path = str_replace('/./', '/', $path);

        // Pattern that matches a dirname followed by a double dot
        $pattern = '/\/.[^\.\/]+\/\.\./';
        // While this pattern is matched...
        while (preg_match($pattern, $path)) {
            // Remove the dirname and double dot, moving up one dir in the path.
            $path = preg_replace($pattern, '', $path);
        }

        return $path;
    }

    /**
     * Retrieve data from array, throwing an exception if not key is not present.
     *
     * @param $data
     * @param $key
     * @throws ParseException
     * @return mixed
     */
    private function requireValue(array $data, $key)
    {
        if (!isset($data[$key])) {
            throw new ParseException('Malformed url, could not find part: "' . $key . '"');
        }

        return $data[$key];
    }

    /**
     * Retrieve data from array, defaulting to null if key not present.
     *
     * @param $data
     * @param $key
     * @return mixed
     */
    private function nullCoalesce(array $data, $key)
    {
        return (isset($data[$key]) ? $data[$key] : null);
    }
}