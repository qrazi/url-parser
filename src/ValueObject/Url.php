<?php

namespace Sivlingworkz\ValueObject;

use Sivlingworkz\Comparable;
use Sivlingworkz\ComparableException;

/**
 * Immutable object representing an Url.
 */
final class Url implements Comparable
{
    /**
     * @var string
     */
    private $scheme;

    /**
     * @var string
     */
    private $host;

    /**
     * @var int
     */
    private $port;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $query;

    /**
     * @var string
     */
    private $fragment;

    /**
     * @param $scheme
     * @param $host
     * @param $port
     * @param $username
     * @param $password
     * @param $path
     * @param $query
     * @param $fragment
     */
    public function __construct(
        $scheme,
        $host,
        $port,
        $username,
        $password,
        $path,
        $query,
        $fragment
    ) {
        $this->scheme = $scheme;
        $this->host = $host;
        $this->port = $port;
        $this->username = $username;
        $this->password = $password;
        $this->path = $path;
        $this->query = $query;
        $this->fragment = $fragment;
    }

    /**
     * @return string
     */
    public function getFullUrl()
    {
        // If no scheme is set, default to url without scheme.
        $fullUrl = ($this->getScheme() ? $this->getScheme() . '://' : '');

        // Append the user and password if present. Never add password without user.
        if ($this->getUsername()) {
            $fullUrl .= $this->getUsername();
            if ($this->getPassword()) {
                $fullUrl .= ':' . $this->getPassword();
            }
            $fullUrl .= '@';
        }

        // The host should always be present
        $fullUrl .= $this->getHost();

        // Append the port if present
        $fullUrl .= ($this->getPort() ? ':' . $this->getPort() : '');

        // Append the path if present
        $fullUrl .= ($this->getPath() ? $this->getPath() : '');

        // Append the query if present
        $fullUrl .= ($this->getQuery() ? '?' . $this->getQuery() : '');

        // Append the fragment if present
        $fullUrl .= ($this->getFragment() ? '#' . $this->getFragment() : '');

        return $fullUrl;
    }

    /**
     * @return string
     */
    public function getScheme()
    {
        return $this->scheme;
    }

    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return int
     */
    public function getPort()
    {
        return $this->port;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @return string
     */
    public function getFragment()
    {
        return $this->fragment;
    }

    /**
     * @inheritDoc
     */
    public function compareTo($other)
    {
        if (!$other instanceof Url) {
            throw new ComparableException('Expected instance of "Url"');
        }

        // Compare VO's based on the full url. Use strcasecmp to do a case-
        // insensitive alphabetical compare.
        $compare = strcasecmp($this->getFullUrl(), $other->getFullUrl());

        if ($compare > 0) {
            // $this is more then $other
            return 1;
        } else {
            if ($compare < 0) {
                // $this is less then $other
                return -1;
            }
        }

        // Equals
        return 0;
    }

    /**
     * Define the magic __set-function to prevent consumer code from setting
     * properties on an instance of this class.
     *
     * @param $field
     * @param $value
     */
    public function __set($field, $value)
    {
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getFullUrl();
    }
}