<?php

namespace Sivlingworkz\ValueObject;

use Sivlingworkz\ComparableException;

/**
 * @group ValueObject
 * @group ValueObject Url
 */
class UrlTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test two ValueObjects compare as equal.
     */
    public function testEqualAsOther()
    {
        $url = new Url('http', 'www.example.com', null, null, null, null, null,
            null);
        $other = new Url('http', 'www.example.com', null, null, null, null,
            null,
            null);

        $this->assertEquals(0, $url->compareTo($other));
    }

    /**
     * Test two ValueObjects compare as not equal.
     */
    public function testMoreThenOther()
    {
        $url = new Url('http', 'www.example.com', null, null, null, null, null,
            null);
        $other = new Url('http', 'subdomain.example.com', null, null, null,
            null, null,
            null);

        $this->assertEquals(1, $url->compareTo($other));
    }

    /**
     * Test two ValueObjects compare as not equal.
     */
    public function testLessThenOther()
    {
        $url = new Url('http', 'www.example.com', null, null, null, null, null,
            null);
        $other = new Url('http', 'www.google.com', null, null, null,
            null, null,
            null);

        $this->assertEquals(-1, $url->compareTo($other));
    }

    /**
     * Test invalid compare when unexpected type is compared.
     *
     * @expectedException \Sivlingworkz\ComparableException
     */
    public function testInvalidOther()
    {
        $url = new Url('http', 'www.example.com', null, null, null, null, null,
            null);
        $other = new \stdClass();

        $url->compareTo($other);
    }
}
