<?php

namespace Sivlingworkz\Parser;

use Sivlingworkz\Validation\UrlValidator;

/**
 * @group Parser
 * @group Parser UrlParser
 */
class UrlTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @dataProvider validUrlProvider
     *
     * @param $url
     * @param $scheme
     * @param $host
     * @param $port
     * @param $username
     * @param $password
     * @param $path
     * @param $query
     * @param $fragment
     */
    public function testParseValidUrls(
        $url,
        $scheme,
        $host,
        $port,
        $username,
        $password,
        $path,
        $query,
        $fragment
    ) {
        $parser = new UrlParser(new UrlValidator());

        $urlObject = $parser->parseUrl($url);

        $this->assertEquals($url, (string)$urlObject);
        $this->assertEquals($scheme, $urlObject->getScheme());
        $this->assertEquals($host, $urlObject->getHost());
        $this->assertEquals($port, $urlObject->getPort());
        $this->assertEquals($username, $urlObject->getUsername());
        $this->assertEquals($password, $urlObject->getPassword());
        $this->assertEquals($path, $urlObject->getPath());
        $this->assertEquals($query, $urlObject->getQuery());
        $this->assertEquals($fragment, $urlObject->getFragment());
    }

    /**
     * Helper function to provide valid urls to test against.
     *
     * @return array
     */
    public function validUrlProvider()
    {
        return array(
            array(
                '/foo/bar/index.php?fooz=baz#showall',
                null,
                null,
                null,
                null,
                null,
                '/foo/bar/index.php',
                'fooz=baz',
                'showall',
            ),
            array(
                'http://johndoe:pa$$word@www.example.com:443/foo/bar.html?fooz=baz#fragment',
                'http',
                'www.example.com',
                443,
                'johndoe',
                'pa$$word',
                '/foo/bar.html',
                'fooz=baz',
                'fragment',
            ),
            array(
                'http://www.example.com/',
                'http',
                'www.example.com',
                null,
                null,
                null,
                '/',
                null,
                null,
            ),
            array(
                'http://www.example.com',
                'http',
                'www.example.com',
                null,
                null,
                null,
                null,
                null,
                null,
            ),
            array(
                'http://www.example.com/foo/bar/test.html',
                'http',
                'www.example.com',
                null,
                null,
                null,
                '/foo/bar/test.html',
                null,
                null,
            ),
            array(
                'http://www.example.com/?foo=bar',
                'http',
                'www.example.com',
                null,
                null,
                null,
                '/',
                'foo=bar',
                null,
            ),
            array(
                'http://www.example.com:8080/test.html',
                'http',
                'www.example.com',
                8080,
                null,
                null,
                '/test.html',
                null,
                null,
            ),
        );
    }

    /**
     * @dataProvider invalidUrlProvider
     * @param $url
     *
     * @expectedException \Sivlingworkz\Parser\ParseException
     *
     * @group develop
     */
    public function testParseInvalidUrl($url)
    {
        $parser = new UrlParser(new UrlValidator());

        $parser->parseUrl($url);
    }

    /**
     * Helper function to provide invalid urls to test against.
     *
     * @return array
     */
    public function invalidUrlProvider()
    {
        return array(
            array('',),
            array('http:www.example.com',),
            array('http:/www.example.com',),
            array('foo',),
            array('://www.example.com',),
            array('//www.example.com',),
        );
    }

    /**
     * @dataProvider dottedPathsProvider
     * @param $dottedPath
     * @param $normalizedPath
     */
    public function testNormalization($dottedPath, $normalizedPath)
    {
        $parser = new UrlParser(new UrlValidator());

        $url = $parser->parseUrl($dottedPath);

        $this->assertEquals($normalizedPath, (string)$url);
    }

    /**
     * Helper function to provide URL's with dotted paths.
     *
     * @return array
     */
    public function dottedPathsProvider()
    {
        return array(
            array(
                'http://www.example.com/foo/bar/./fooz',
                'http://www.example.com/foo/bar/fooz',
            ),
            array(
                'http://www.example.com/foo/bar/../fooz',
                'http://www.example.com/foo/fooz',
            ),
            array(
                'http://www.example.com/foo/bar/../index.php',
                'http://www.example.com/foo/index.php',
            ),
            array(
                'http://www.example.com/foo/bar/fooz/baz/../../../../index.php',
                'http://www.example.com/index.php',
            ),
            array(
                'http://www.example.com/foo/./bar/fooz/baz/../../../../index.php',
                'http://www.example.com/index.php',
            ),
        );
    }
}